const puppeteer = require('puppeteer');

(async () => {
    const browser = await puppeteer.launch({ headless: false });
    const page = await browser.newPage();
    await page.goto('https://scrapethissite.com/pages/forms/');
    await page.screenshot({ path: 'vps.png' });
    // await browser.waitForTarget(() => false);

    const result = await page.evaluate(() => {
        const grabfrmrow = (row, classname) => row.querySelector(`td.${classname}`).innerText.trim()
        const teamSelector = 'tr.team'
        const data = []

        const teamRows = document.querySelectorAll(teamSelector)

        for (const tr of teamRows) {
            data.push({
                name: grabfrmrow(tr, 'name')
            })
        }
        return data;
    });
    console.log(result);
    await browser.close();


})();